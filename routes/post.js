
var restful = require('node-restful'),
  Post = require('./../models/post');

var postRoutes = restful.model('Post', Post)
  .methods(['get', 'delete', 'put', 'post' ]);


module.exports = postRoutes;
