
var restful = require('node-restful'),
  User = require('./../models/user');

var userRoutes = restful.model('User', User)
  .methods(['get', 'delete', 'put', 'post' ]);

/*

/users [GET]
/users [POST]
/users/:id [GET]
/users/:id [PUT]
/users/:id [DELETE]
*/

//userRoutes.route('add-project', 'post', function(req, res, next) {
  // Add a project to a user...
//});

// userRoutes.before('get', function pop(req, res, next) {
//   if (req && req.query && req.query.populate) {
//     req.quer.populate(req.query.populate);
//   }
//   next();
// });


module.exports = userRoutes;
