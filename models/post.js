'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var PostSchema = new Schema({
    body: {
        type: 'string',
        required: true
    },
    user: {
        type: 'ObjectId',
        ref: 'User',
        required: true
    }
});

module.exports = PostSchema;
