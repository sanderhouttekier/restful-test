'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var genders = ['male', 'female', 'x'];
var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var validateUniqueEmail = function(value, callback) {
  var User = mongoose.model('User');
  User.find({
    $and: [{
      email: value
    }, {
      _id: {
        $ne: this._id
      }
    }]
  }, function(err, user) {
    callback(err || user.length === 0);
  });
};

var UserSchema = new Schema({
    lastName: {
        type: 'string',
        required: true
    },
    firstName: {
        type: 'string',
        required: true
    },
    gender: {
        type: 'string',
        required: true,
        enum: genders,
        default: 'x'
    },
    avatar: {
        type: 'string'
    },
    email: {
        type: 'string',
        required: true,
        unique: true,
        lowercase: true,
        // Regexp to validate emails with more strict rules as added in tests/users.js which also conforms mostly with RFC2822 guide lines
        match: [emailRegex, 'Please enter a valid email'],
        validate: [validateUniqueEmail, 'E-mail address is already in-use']
    },
    age: {
      type: 'number',
      required: true
    },
    // direct reference works on .populate('boss') or ?populate=boss
    boss: {
        type: 'ObjectId',
        ref: 'User'
    },
    // direct reference DOES NOT WORK on .populate('posts') or ?populate=posts
    posts: {
        type: ['ObjectId'],
        ref: 'Post'
    },
    // direct reference works on .populate('lists') or ?populate=lists
    lists: [{
        type: 'ObjectId',
        ref: "Post"
    }]
});

module.exports = UserSchema;
