var express = require('express'),
    mongoose = require('mongoose'),
    restful = require('node-restful'),
    app = express(),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 5002,
    User = require('./routes/user'),
    Post = require('./routes/post');

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    mongoose.connect('mongodb://localhost:27017/restful-test');

    // ROUTES FOR OUR API
    var router = express.Router();

    // route middleware that will log every request
    router.use(function(req, res, next) {
        console.log(req.method, req.url);
        next();
    });

    app.get('/', function function_name (req, res, next) {
        res.json({'msg': 'hello world!'});
    });

    User.register(app, '/users');
    Post.register(app, '/posts');

    // REGISTER OUR ROUTES -------------------------------
    // all of our routes will be prefixed with /api
    app.use('/', router);

    // START THE SERVER
    // =============================================================================
    app.listen(port);
    console.log('Magic happens on port ' + port);
